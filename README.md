Share With NewPipe (forked)
===========================

> This app allows to open YouTube videos with [NewPipe](https://github.com/theScrabi/NewPipe) using the share menu of another app.


This repo is a fork of Poussinou's [ShareWithNewPipe](https://github.com/Poussinou/ShareWithNewPipe).

This version adds Soundcloud url compatibility and runs perfectly with new versions of NewPipe (0.15.0)

#### TODO:
* Make this program into a youtube url android default opener